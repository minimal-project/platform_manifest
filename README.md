<div align="center">
<img src="https://gitlab.com/minimal-project/Docs/raw/ten/MinimalProjectLargeBlank.png">
</div>

Introduction
---------------
A AOSP/CAF project designed with the idea of ​​keeping everything clean and "minimal". Without neglecting the necessary updates to maintain a stable, secure and fast system.

Sync
---------------

```bash
repo init -u https://gitlab.com/minimal-project/platform_manifest.git -b ten
```
Then to sync up:
```bash
repo sync -f --force-sync --no-tags --no-clone-bundle
```

Building
---------------

```bash
. build/envsetup.sh
lunch minimal_devicename-userdebug
mka bacon
```

Credits
---------------

- LineageOS
- SuperiorOS
- AEX
- Syberia
- LiquidRemix
- ArrowOS
- AOSiP
- PixelExperience
- And to anyone who has crossed on my way and offered their help, thanks!
